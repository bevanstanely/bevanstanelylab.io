---
# Display name
title: Bevan Stanely

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Student of Biological Sciences

# Organizations/Affiliations
organizations:
- name: Indian Institute of Science
  url: "https://www.iisc.ac.in/"

# Short bio (displayed in user profile at end of posts)
bio: I am inspire by complex systems. I see a lot of them. I am aiming to understand them better using mathematical and computational approach. I hope in time I get to play with some of the more advanced questions in Physics as well.

interests:
- Complex Systems
- Programming
- Mathematics

education:
  courses:
  - course: Integrated PhD in Biological Science
    institution: Indian Institute of Science
    year: now
  - course: BSc in Biological Sciences
    institution: Bangalore University

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:bevanstanely@iisc.ac.in'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/8ev_Ash
#- icon: google-scholar
#  icon_pack: ai
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/chain-ed-reaction
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

Bevan Stanely is a student of biology transitioning to more theoretical sciences. He loves programming and playing with mathematical reasoning. He also does a lot of agent based modeling.
